(ns plf02.core)
;;1-5
(defn función-associative?-1
  [cs]
  (associative? cs))
(defn función-associative?-2
  [cs]
  (associative? cs))
(defn función-associative?-3
  [cs]
  (associative? cs))
(función-associative?-1 [" A" "B" "C"])
(función-associative?-2 '([" A" "B" "C"]))
(función-associative?-3 {:a "HOLA" :b "MUNDO" :c "Clojure"})

(defn función-boolean?-1
  [x]
  (boolean? x))
(defn función-boolean?-2
  [x]
  (boolean? x))
(defn función-boolean?-3
  [x]
  (boolean? x))
(función-boolean?-1 "false")
(función-boolean?-2 nil)
(función-boolean?-3 false)

(defn función-char?-1
  [x]
  (char? x))
(defn función-char?-2
  [x]
  (char? x))
(defn función-char?-3
  [x]
  (char? x))
(función-char?-1 "A")
(función-char?-2 [12 12])
(función-char?-3 \A)

(defn función-coll?-1
  [xs]
  (coll? xs))
(defn función-coll?-2
  [xs]
  (coll? xs))
(defn función-coll?-3
  [xs]
  (coll? xs))
(función-coll?-1 [])
(función-coll?-2 "1 2 3")
(función-coll?-3 {:ms {} :vs [] :a "A" })

(defn función-decimal?-1
  [n]
  (decimal? n))
(defn función-decimal?-2
  [n]
  (decimal? n))
(defn función-decimal?-3
  [n]
  (decimal? n))

(función-decimal?-1 1/2)
(función-decimal?-2 1.0)
(función-decimal?-3 999M)

;;6-10
(defn función-double?-1
  [n]
  (double? n))
(defn función-double?-2
  [n]
  (double? n))
(defn función-double?-3
  [n]
  (double? n))
(función-double?-1 0.5)
(función-double?-2 1/2)
(función-double?-3 100M)

(defn función-float?-1
  [n]
  (float? n))
(defn función-float?-2
  [n]
  (float? n))
(defn función-float?-3
  [n]
  (float? n))
(función-float?-1 0.2)
(función-float?-2 2020)
(función-float?-3 20/4)

(defn función-ident?-1
  [x]
  (ident? x))
(defn función-ident?-2
  [x]
  (ident? x))
(defn función-ident?-3
  [x]
  (ident? x))
(función-ident?-1 "Z")
(función-ident?-2 'XYZ)
(función-ident?-3 :Z)

(defn función-indexed?-1
  [cs]
  (indexed? cs))
(defn función-indexed?-2
  [cs]
  (indexed? cs))
(defn función-indexed?-3
  [cs]
  (indexed? cs))
(función-indexed?-1 [1 2 3])
(función-indexed?-2 {:a \A :b \B :c \C :d \D})
(función-indexed?-3 '(10 20 30 40))

(defn función-int?-1
  [x]
  (int? x))
(defn función-int?-2
  [x]
  (int? x))
(defn función-int?-3
  [x]
  (int? x))
(función-int?-1 12)
(función-int?-2 12M)
(función-int?-3 23.4)
;;11-15
(defn función-integer?-1
  [n]
  (integer? n))
(defn función-integer?-2
  [n]
  (integer? n))
(defn función-integer?-3
  [n]
  (integer? n))
(función-integer?-1 "1")
(función-integer?-2 1.000001)
(función-integer?-3 901000)

(defn función-keyword?-1
  [x]
  (keyword? x))
(defn función-keyword?-2
  [x]
  (keyword? x))
(defn función-keyword?-3
  [x]
  (keyword? x))
(función-keyword?-1 :key1)
(función-keyword?-2 {:A "A"})
(función-keyword?-3 "key1")

(defn función-list?-1
  [cs]
  (list? cs))
(defn función-list?-2
  [cs]
  (list? cs))
(defn función-list?-3
  [cs]
  (list? cs))
(función-list?-1 '("A" [] {} ))
(función-list?-2 (list [] [] []))
(función-list?-3 [list 12 121 2])

(defn función-map-entry?-1
  [x]
  (map-entry? x))
(defn función-map-entry?-2
  [x]
  (map-entry? x))
(defn función-map-entry?-3
  [x]
  (map-entry? x))
(función-map-entry?-1 {:a "HOLA" :b "GOOGLE"})
(función-map-entry?-2 (first {:a "HOLA" :b "Google"}))
(función-map-entry?-3 :A)

(defn función-map?-1
  [x]
  (map? x))
(defn función-map?-2
  [x]
  (map? x))
(defn función-map?-3
  [x]
  (map? x))
(función-map?-1 {})
(función-map?-2 [1 2])
(función-map?-3 (hash-map :k1 9090))

;;16-20
(defn función-nat-int?-1
  [x]
  (nat-int? x))
(defn función-nat-int?-2
  [x]
  (nat-int? x))
(defn función-nat-int?-3
  [x]
  (nat-int? x))
(función-nat-int?-1 0)
(función-nat-int?-2 -1010)
(función-nat-int?-3 0.000001)

(defn función-number?-1
  [x]
  (number? x))
(defn función-number?-2
  [x]
  (number? x))
(defn función-number?-3
  [x]
  (number? x))
(función-number?-1 -0.00000004)
(función-number?-2 "2020")
(función-number?-3 40/20)

(defn función-pos-int?-1
  [x]
  (pos-int? x))
(defn función-pos-int?-2
  [x]
  (pos-int? x))
(defn función-pos-int?-3
  [x]
  (pos-int? x))
(función-pos-int?-1 -32)
(función-pos-int?-2 10.0)
(función-pos-int?-3 129909)

(defn función-ratio?-1
  [n]
  (ratio? n))
(defn función-ratio?-2
  [n]
  (ratio? n))
(defn función-ratio?-3
  [n]
  (ratio? n))
(función-ratio?-1 2.90)
(función-ratio?-2 10/50)
(función-ratio?-3 22)

(defn función-rational?-1
  [n]
  (rational? n))
(defn función-rational?-2
  [n]
  (rational? n))
(defn función-rational?-3
  [n]
  (rational? n))
(función-rational?-1 100)
(función-rational?-2 1/3)
(función-rational?-3 0.333)

;;21-24
(defn función-seq?-1
  [x]
  (seq? x))
(defn función-seq?-2
  [x]
  (seq? x))
(defn función-seq?-3
  [x]
  (seq? x))
(función-seq?-1 '(1 2 3))
(función-seq?-2 [10 60 90])
(función-seq?-3 (seq [10 60 90]))

(defn función-seqable?-1
  [x]
  (seqable? x))
(defn función-seqable?-2
  [x]
  (seqable? x))
(defn función-seqable?-3
  [x]
  (seqable? x))
(función-seqable?-1 '(60 70 90))
(función-seqable?-2 [1 2 3])
(función-seqable?-3 100)

(defn función-sequential?-1
  [cs]
  (sequential? cs))
(defn función-sequential?-2
  [cs]
  (sequential? cs))
(defn función-sequential?-3
  [cs]
  (sequential? cs))
(función-sequential?-1 #{100 200 300})
(función-sequential?-2 [#{100 200 300} #{ 400 500 600}])
(función-sequential?-3 '())

(defn función-set?-1
  [x]
  (set? x))
(defn función-set?-2
  [x]
  (set? x))
(defn función-set?-3
  [x]
  (set? x))
(función-set?-1 ["X" "Y" "Z"])
(función-set?-2 #{1.1 0.1 0.2 0.3 0.4})
(función-set?-3 (hash-set 9 8 7 6) )

;;25-28
(defn función-some?-1
  [x]
  (some? x))
(defn función-some?-2
  [x]
  (some? x))
(defn función-some?-3
  [x]
  (some? x))
(función-some?-1 "")
(función-some?-2 nil)
(función-some?-3 :k1)

(defn función-string?-1
  [x]
  (string? x))
(defn función-string?-2
  [x]
  (string? x))
(defn función-string?-3
  [x]
  (string? x))
(función-string?-1 "Hola mundo")
(función-string?-2 '("Danny " "Jose" "Kary"))
(función-string?-3 12)

(defn función-symbol?-1
  [x]
  (symbol? x))
(defn función-symbol?-2
  [x]
  (symbol? x))
(defn función-symbol?-3
  [x]
  (symbol? x))
(función-symbol?-1 'HI)
(función-symbol?-2 '('HI))
(función-symbol?-3 :k1)

(defn función-vector?-1
  [x]
  (vector? x))
(defn función-vector?-2
  [x]
  (vector? x))
(defn función-vector?-3
  [x]
  (vector? x))
(función-vector?-1 [{} {} {}])
(función-vector?-2 '(["A" "X" "Z"]))
(función-vector?-3 (vector 10 30 60 70))

;;Funciones de orden superior
;;29-33

(defn función-drop-1
  [n cs]
  (drop n cs))
(defn función-drop-2
  [n cs]
  (drop n cs))
(defn función-drop-3
  [n cs]
  (drop n cs))

(función-drop-1 0 [1 2 3 4])
(función-drop-2 1 #{10 50 61 2})
(función-drop-3 5 '(9 10 "A" "B"))

(defn función-drop-last-1
  [cs]
  (drop-last cs))
(defn función-drop-last-2
  [n cs]
  (drop-last n cs))
(defn función-drop-last-3
  [n cs]
  (drop-last n cs))
(función-drop-last-1 [90 10 "Hola" "Clojure"])
(función-drop-last-2 2 (hash-set "A" "X" "Y"))
(función-drop-last-3 3 #{10 291 \A \B})

(defn función-drop-while-1
  [pred cs]
  (drop-while pred cs))
(defn función-drop-while-2
  [pred cs]
  (drop-while pred cs))
(defn función-drop-while-3
  [pred cs]
  (drop-while pred cs))
(función-drop-while-1 neg? [-1 -2 2 3 10 50])
(función-drop-while-2 pos? '(1 2 3 90 5 -10 -32))
(función-drop-while-3 even? [10 20 30 -2 5])

(defn función-every?-1
  [pred cs]
  (every? pred cs))
(defn función-every?-2
  [pred cs]
  (every? pred cs))
(defn función-every?-3
  [pred cs]
  (every? pred cs))
(función-every?-1 pos? [1 2 3 5 6])
(función-every?-2 {:k1 "UNO" :k2 "DOS"} {:k1 1 :k2 2})
(función-every?-3 false? #{})

(defn función-filterv?-1
  [pred cs]
  (filterv pred cs))
(defn función-filterv?-2
  [pred cs]
  (filterv pred cs))
(defn función-filterv?-3
  [pred cs]
  (filterv pred cs))
(función-filterv?-1 zero? '(1 2 3 0 0 1 2 3))
(función-filterv?-2 odd? #{1 2 3 4 5 6 7 9 0} )
(función-filterv?-3 (fn [x] (and (> x 15) (< x 23))) [10 23 21 22 4 15 16 17])
;;34-38

(defn función-group-by?-1
  [f cs]
  (group-by f cs))
(defn función-group-by?-2
  [f cs]
  (group-by f cs))
(defn función-group-by?-3
  [f cs]
  (group-by f cs))
(función-group-by?-1 even? '(1 2 3 4 10 33 2123 123 22 1))
(función-group-by?-2 first ["A" "B" "C"])
(función-group-by?-3 count {:k1 "HOLA" :K2 "MUNDO" :K3 "EARTH"})

(defn función-iterate-1
  [f x]
  (iterate f x))
(defn función-iterate-2
  [f x]
  (iterate f x))
(defn función-iterate-3
  [f x]
  (iterate f x))
(función-iterate-1 inc 3 )
(función-iterate-2 dec 20)
(función-iterate-3 inc -10 )

(defn función-keep-1
  [f cs]
  (keep f cs))
(defn función-keep-2
  [f cs]
  (keep f cs))
(defn función-keep-3
  [f cs]
  (keep f cs))
(función-keep-1 seq [{:a :B} '(1 2) {} []] )
(función-keep-2 rational? [1 2 1/2 0.5 21 -1])
(función-keep-3 {:k1 1 :k2 2 :k4 4} [:k1 :k4])

(defn función-keep-indexed-1
  [f cs]
  (keep-indexed f cs))
(defn función-keep-indexed-2
  [f cs]
  (keep-indexed f cs))
(defn función-keep-indexed-3
  [f cs]
  (keep-indexed f cs))

(función-keep-indexed-1 (fn [x y] (if (pos? y) x)) [1 2 3 -5 -2 -1 10])
(función-keep-indexed-2 (fn [x y] (if (neg? y) x)) '(-2 -10 10 20 30 -2))
(función-keep-indexed-3 (fn [x y] (if (string? y) x )) [1 2 3 "A" "B"])

(defn función-map-indexed-1
  [f cs]
  (map-indexed f cs))
(defn función-map-indexed-2
  [f cs]
  (map-indexed f cs))
(defn función-map-indexed-3
  [f cs]
  (map-indexed f cs))
(función-map-indexed-1 vector "ALEXA")
(función-map-indexed-2 hash-map '(:k1 10 :k2 20 :k3 30))
(función-map-indexed-3 list [1 2 3 4 10 -1 1/2])

;;39-43
(defn función-mapcat-1
  [f cs]
  (mapcat f cs))
(defn función-mapcat-2
  [f cs ds]
  (mapcat f cs ds))
(defn función-mapcat-3
  [f cs ds es]
  (mapcat f cs ds es))
(función-mapcat-1 reverse [[10 9 8 7 6 5 ] [1 2 3 4]])
(función-mapcat-2 list [1 2] ["A" "B"])
(función-mapcat-3 list '(:k1 ) [100 200] [300])

(defn función-mapv-1
  [f cs]
  (mapv f cs))
(defn función-mapv-2
  [f cs cs2]
  (mapv f cs cs2))
(defn función-mapv-3
  [f cs cs2 cs3]
  (mapv f cs cs2 cs3))
(función-mapv-1 dec '(10 20 30 -1))
(función-mapv-2 * '(10 20 30 -1) [1 2 3 4])
(función-mapv-3 str ["HI "] ["MR "] #{"DANNY"} )

(defn función-merge-with-1
  [f mp]
  (merge-with f mp))
(defn función-merge-with-2
  [f mp mp2]
  (merge-with f mp mp2))
(defn función-merge-with-3
  [f mp mp2 mp3]
  (merge-with f mp mp2 mp3))

(función-merge-with-1 into {"HOLA" ["MUNDO" "EARTH"] 
                            "HI"   ["MR" ["STARK"]]
                            })
(función-merge-with-2 + {:k1 "k1"} {:k2 "k2"})
(función-merge-with-3 - {:a 10 } {:a 20} {:a 30})

(defn función-not-any?-1
  [pred cs]
  (not-any? pred cs))
(defn función-not-any?-2
  [pred cs]
  (not-any? pred cs))
(defn función-not-any?-3
  [pred cs]
  (not-any? pred cs))
(función-not-any?-1 even? [1 3 9 7])
(función-not-any?-2 string? #{" H" "J" :k1})
(función-not-any?-3 boolean? '(true false "true"))

(defn función-not-every?-1
  [pred cs]
  (not-every? pred cs))
(defn función-not-every?-2
  [pred cs]
  (not-every? pred cs))
(defn función-not-every?-3
  [pred cs]
  (not-every? pred cs))
(función-not-every?-1 int? [0 1 2 "A" ])
(función-not-every?-2 char? #{\A \B \C})
(función-not-every?-3 double? '(1 1.0 1/1 0.1 1.1))

;;44-48

(defn función-partition-by-1
  [f cs]
  (partition-by f cs))
(defn función-partition-by-2
  [f cs]
  (partition-by f cs))
(defn función-partition-by-3
  [f cs]
  (partition-by f cs))
(función-partition-by-1 identity "HOOLAAAH")
(función-partition-by-2 count ["a" "x" "y" "jkl" "mno" "pq" "rs"] )
(función-partition-by-3 number? [1 2 3 "A" "K" \F]  )

(defn función-reduce-kv-1
  [f init cs]
  (reduce-kv f init cs))
(defn función-reduce-kv-2
  [f init cs]
  (reduce-kv f init cs))
(defn función-reduce-kv-3
  [f init cs]
  (reduce-kv f init cs))

(función-reduce-kv-1 assoc {} [:a 1 :b 2 :c 3])
(función-reduce-kv-2 (fn [x y z] (assoc x y z)) {} ["AF" "AJ" "AL"])
(función-reduce-kv-3 (fn [x y z] (assoc x (keyword z) (name y))) {} {:uno "1" :dos "2" :tres "3"})
(defn función-remove-1
  [pred cs]
  (remove pred cs))
(defn función-remove-2
  [pred cs]
  (remove pred cs))
(defn función-remove-3
  [pred cs]
  (remove pred cs))
(función-remove-1 nil? '(nil nil 3 4 nil 7))
(función-remove-2 double? [1.0 1.1 0.1 1/2 9 8 7 10])
(función-remove-3 even? #{10 11 12 13 14 15 16 17 18})

(defn función-reverse-1
  [s]
  (reverse s))
(defn función-reverse-2
  [s]
  (reverse s))
(defn función-reverse-3
  [s]
  (reverse s))
(función-reverse-1 "reconocer")
(función-reverse-2 [1 2 3 4])
(función-reverse-3 {:k1 1 :k2 2 :k3 3 :k4 4})

(defn función-some-1
  [pred cs]
  (some pred cs))
(defn función-some-2
  [pred cs]
  (some pred cs))
(defn función-some-3
  [pred cs]
  (some pred cs))
(función-some-1 even? [1 2 3 4])
(función-some-2 odd? '(0 0 0 2 8))
(función-some-3 string? #{"A" 12 3})

;;49-56
(defn función-sort-by-1
  [f cs]
  (sort-by f cs))
(defn función-sort-by-2
  [f cs]
  (sort-by f cs))
(defn función-sort-by-3
  [f cs]
  (sort-by f cs))

(función-sort-by-1 count ["ABCD" "E" "FG" "H"])
(función-sort-by-2 first [[1 "A"] [2 "B"] [3 "C"]])
(función-sort-by-3 :k1 [{:k1 5} {:k1 10} {:k1 1} {:k1 -1}])

(defn función-split-with-1
  [pred cs]
  (split-with pred cs))
(defn función-split-with-2
  [pred cs]
  (split-with pred cs))
(defn función-split-with-3
  [pred cs]
  (split-with pred cs))
(función-split-with-1 pos? [1 2 3 -1 -2 -3])
(función-split-with-2 neg? [-1 -1/2 2 3 0.01 ])
(función-split-with-3 odd? '(1 3 7 9 2 4 8))

(defn función-take-1
  [n cs]
  (take n cs))
(defn función-take-2
  [n cs ]
  (take n cs))
(defn función-take-3
  [n cs]
  (take n cs))
(función-take-1 5 [1 2])
(función-take-2 0 '(1 2 3))
(función-take-3 3 #{10 20 30 40 50})

(defn función-take-last-1
  [n cs]
  (take-last n cs))
(defn función-take-last-2
  [n cs]
  (take-last n cs))
(defn función-take-last-3
  [n cs]
  (take-last n cs))
(función-take-last-1 1 [10 20 \A])
(función-take-last-2 3 '("A" "X" "Y" 1 5 6))
(función-take-last-3 2 #{[] "a" 5 1.0})

(defn función-take-nth-1
  [n cs]
  (take-nth n cs))
(defn función-take-nth-2
  [n cs]
  (take-nth n cs))
(defn función-take-nth-3
  [n cs]
  (take-nth n cs))
(función-take-nth-1 5 [10 20 30 40 50 60 70])
(función-take-nth-2 2 "ABCDEF")
(función-take-nth-3 3 '(-1 -2 -3 -4 -5))

(defn función-take-while-1
  [pred cs]
  (take-while pred cs))
(defn función-take-while-2
  [pred cs]
  (take-while pred cs))
(defn función-take-while-3
  [pred cs]
  (take-while pred cs))
(función-take-while-1 string? '("X" "Y" "Z" \A \B))
(función-take-while-2 char? [\1 \2 "A" "B"])
(función-take-while-3 pos-int? [1 2 3 0.1 -1])

(defn función-update-1
  [x y z]
  (update x y z))
(defn función-update-2
  [w x y]
  (update w x y))
(defn función-update-3
  [v w x z]
  (update v w x z))
(función-update-1 {:k1 90} :k1 inc)
(función-update-2 [10 20 30] 2 dec)
(función-update-3 {:k2 2 :k3 3} :k3 + 50)

(defn función-update-in-1
  [x y z]
  (update-in x y z))
(defn función-update-in-2
  [x y z]
  (update-in x y z))
(defn función-update-in-3
  [w  x y z]
  (update-in w x y z))
(función-update-in-1 {:mes 11 :año 2020} [:año] inc)
(función-update-in-2 {:mes 13 :año 2011} [:mes] dec)
(función-update-in-3 {:k1 10} [:k1] - 10)